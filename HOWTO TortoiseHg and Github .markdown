Read-only access, in short
--------------------------
First,

    hg clone http://bitbucket.org/durin42/hg-git HGGIT_PATH

Second, in "[mercurial.ini](http://www.selenic.com/mercurial/hgrc.5.html#files)" add:

    [extensions]
    hggit=HGGIT_PATH\hggit

    [ui]
    ssh = "C:\Program Files\TortoiseHg\TortoisePlink.exe"

Third, for read-only access to a github repo:

    hg clone git://github.com/USER/PROJECT.git LOCAL_PATH

Read-write access
-----------------

For proper pushing, the ["bookmarks" extension](http://mercurial.selenic.com/wiki/BookmarksExtension) needs to be enabled in "mercurial.ini":

    [extensions]
    bookmarks=

Second, run

    copy "C:\Program Files\TortoiseHg\TortoisePlink.exe" "C:\Program Files\TortoiseHg\ssh.exe"`

and also make a related change in "mercurial.ini":

    [ui]
    ssh = "C:\Program Files\TortoiseHg\ssh.exe"

Proper public+private key pair must be created, with the public key uploaded to github (see the links below).

PuTTY tool "pageant" must be started, with the private key loaded.

I needed to re-clone the repository, with:

    hg clone git+ssh://git@github.com/USER/PROJECT.git LOCAL_PATH_2

(I pulled the commits from LOCAL_PATH into this new repo)

Although the bookmarks extension is "automatic", for me it required manual tweaking on each push, especially when there were branches in the repo; I had to move the bookmark-tags to be positioned on most recent changesets on proper heads.


References
----------
  * http://jamesmckay.net/2010/06/tortoisehg-as-a-github-client-on-windows/
  * http://bitbucket.org/durin42/hg-git/
  * http://stackoverflow.com/questions/2573571/cloning-a-read-write-github-repository-using-tortoisehg
  * http://www.jeremyskinner.co.uk/2010/02/18/using-mercurial-with-codeplex-and-github/
  * http://hg-git.github.com/
